package ru.t1.sarychevv.tm.command.system;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Vladimir Sarychev");
        System.out.println("e-mail: vsarychev@t1-consulting.ru");
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show about program.";
    }

}
