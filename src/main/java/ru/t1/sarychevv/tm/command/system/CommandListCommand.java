package ru.t1.sarychevv.tm.command.system;

import ru.t1.sarychevv.tm.api.model.ICommand;
import ru.t1.sarychevv.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandListCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[COMMAND]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public String getName() {
        return "commands";
    }

    @Override
    public String getArgument() {
        return "-cmd";
    }

    @Override
    public String getDescription() {
        return "Show command list.";
    }

}
