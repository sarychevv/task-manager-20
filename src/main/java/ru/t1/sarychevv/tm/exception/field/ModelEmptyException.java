package ru.t1.sarychevv.tm.exception.field;

public final class ModelEmptyException extends AbstractFieldException {

    public ModelEmptyException() {
        super("Error! Model is empty...");
    }

}
