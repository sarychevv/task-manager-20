package ru.t1.sarychevv.tm.api.service;

import ru.t1.sarychevv.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    void create(String name, String description);

}
