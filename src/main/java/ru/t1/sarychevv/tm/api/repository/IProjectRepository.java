package ru.t1.sarychevv.tm.api.repository;

import ru.t1.sarychevv.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}
