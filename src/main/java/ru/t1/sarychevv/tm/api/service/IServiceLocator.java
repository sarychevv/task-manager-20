package ru.t1.sarychevv.tm.api.service;

import ru.t1.sarychevv.tm.api.model.IAuthService;

public interface IServiceLocator {

    ICommandService getCommandService();

    ILoggerService getLoggerService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

    IUserService getUserService();

    IAuthService getAuthService();

}
