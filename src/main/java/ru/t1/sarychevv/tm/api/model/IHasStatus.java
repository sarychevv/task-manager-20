package ru.t1.sarychevv.tm.api.model;

import ru.t1.sarychevv.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
