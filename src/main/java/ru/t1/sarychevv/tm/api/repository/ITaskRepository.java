package ru.t1.sarychevv.tm.api.repository;

import ru.t1.sarychevv.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

}
