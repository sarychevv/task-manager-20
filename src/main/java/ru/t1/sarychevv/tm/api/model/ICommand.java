package ru.t1.sarychevv.tm.api.model;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute();

}
