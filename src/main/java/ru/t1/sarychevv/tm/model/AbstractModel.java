package ru.t1.sarychevv.tm.model;

import ru.t1.sarychevv.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public abstract class AbstractModel {

    protected String id = UUID.randomUUID().toString();

    protected Status status = Status.NOT_STARTED;

    protected String name = "";

    protected String description = "";

    protected Date created = new Date();

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
